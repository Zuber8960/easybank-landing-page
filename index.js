const humburgButton = document.getElementById("humburg");
const humburgNavBar = document.getElementById("humburg-box");
const humburgButtonInnerHTML = document.getElementById("humburg").innerHTML;
let cross = "<h1>X</h1>";
// console.log(humburg);

humburgButton.addEventListener("click" , () => {
    if(humburgNavBar.style.display === "none"){
        document.getElementById("humburg").innerHTML = cross;
        humburgNavBar.style.display = "block";
    }else{
        document.getElementById("humburg").innerHTML = humburgButtonInnerHTML;
        humburgNavBar.style.display = "none";
    }
})


window.addEventListener('resize' ,() => {
    if(window.innerWidth >= 480){
        document.getElementById("humburg").innerHTML = humburgButtonInnerHTML;
        humburgNavBar.style.display = "none";
    }
})

